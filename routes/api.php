<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::get('cashier/balance', 'CashApiController@cargarAperturaCaja');
    Route::post('cashier/balance/open/day', 'CashApiController@guardarAperturaCaja');
    Route::get('has/open/cashier/balance', 'CashApiController@cargarCierreCaja');
    Route::post('cashier/balance/close/day', 'CashApiController@guardarCierreCaja');
    Route::post('cashier/movimientos', 'CashApiController@guardarMovimientos');
});
