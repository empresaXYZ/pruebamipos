<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('expenses')) {
            Schema::create('expenses', function (Blueprint $table) {
                $table->increments('expenses_id');
                $table->integer('cash_id')->unsigned()->nullable();
                $table->string('name');
                $table->integer('value')->unsigned()->nullable();
                $table->char('status', 1)->default('A');
                $table->timestamps();
                $table->foreign('cash_id')->references('cash_id')->on('cash');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
