<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cash')) {
            Schema::create('cash', function (Blueprint $table) {
                $table->increments('cash_id');
                $table->date('date_open');
                $table->time('hour_open');
                $table->integer('value_previous_close')->unsigned()->nullable();
                $table->integer('value_open')->unsigned()->nullable();
                $table->string('observation');
                $table->date('date_close')->nullable();
                $table->time('hour_close')->nullable();
                $table->integer('value_card')->unsigned()->nullable();
                $table->integer('value_cash')->unsigned()->nullable();
                $table->integer('value_close')->unsigned()->nullable();
                $table->integer('value_sales')->unsigned()->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash');
    }
}
