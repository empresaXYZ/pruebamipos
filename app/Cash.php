<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cash extends Model
{
    protected $fillable = [
        'cash_id',
        'date_open',
        'hour_open',
        'value_previous_close',
        'value_open',
        'observation',
        'date_close',
        'hour_close',
        'value_card',
        'value_cash',
        'value_close',
        'value_sales',

    ];
    protected $table = 'cash';
    protected $primaryKey = 'cash_id';
    protected $appends = [
        'value'
    ];
    public function getValueAttribute()
    {
        $expenses = $this->expenses;
        $total_expense = 0;
        foreach ($expenses as $key => $expense) {
            $total_expense = $total_expense + $expense->value;
        }
        return  $this->value_open +  $this->value_cash + $this->value_sales + $this->value_card - $total_expense;
    }
    public function expenses()
    {
        return $this->hasMany('App\Expenses', 'cash_id', 'cash_id');
    }
}
