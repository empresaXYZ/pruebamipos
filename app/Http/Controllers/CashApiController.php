<?php

namespace App\Http\Controllers;

use App\Cash;
use App\Expenses;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CashApiController extends Controller
{
    public function cargarAperturaCaja()
    {

        try {
            $cash = Cash::select('date_open', 'hour_open', 'value_previous_close', 'value_open', 'observation')->latest('date_open')->latest('hour_open')->whereNull('date_close')->first();
            if (!isset($cash) || $cash == null) {
                return  response()->json(['msg' => 'No existe una caja aperturada para poder obtener información.'], 422);
            }
            return  response()->json(['results' => $cash], 200);
        } catch (Exception $e) {
            return response()->json(['mensaje' => $e->getMessage()], 500);
        }
    }
    public function guardarAperturaCaja(Request $request)
    {
        $request->validate([
            'value_open' => 'required|numeric|integer|gt:0',
            'value_previous_close' => 'required|numeric|integer|gt:0',

        ]);
        try {
            $dateTime = new DateTime();
            $fecha = $dateTime->format('Y-m-d');
            $time = $dateTime->format('H:i:s');

            $cashActual = Cash::select('date_close')->latest('date_open')->latest('hour_open')->whereNull('date_close')->first();
            if (isset($cashActual) && $cashActual != null) {
                return  response()->json(['msg' => 'Ya existe una caja aperturada, debe cerrar la caja para aperturar otra.'], 422);
            }
            $cash = Cash::Create(
                [
                    'date_open' => $fecha,
                    'hour_open' => $time,
                    'value_previous_close' => is_null($request->input('value_previous_close')) ? 0 : $request->input('value_previous_close'),
                    'value_open' => is_null($request->input('value_open')) ? 0 : $request->input('value_open'),
                    'observation' => is_null($request->input('observation')) ? '' : $request->input('observation'),
                    'value_card' => is_null($request->input('value_card')) ? 0 : $request->input('value_card'),
                    'value_cash' => is_null($request->input('value_cash')) ? 0 : $request->input('value_cash'),
                    'value_close' => is_null($request->input('value_close')) ? 0 : $request->input('value_close'),
                    'value_sales' => is_null($request->input('value_sales')) ? 0 : $request->input('value_sales'),
                ]
            );
            return  response()->json(['results' => $cash, 'msg' => 'Información guardada con exito'], 200);
        } catch (Exception $e) {
            return response()->json(['mensaje' => $e->getMessage()], 500);
        }
    }
    public function cargarCierreCaja()
    {

        try {
            $cash = Cash::latest('date_open')->latest('hour_open')->first();
            if (!isset($cash) || $cash == null) {
                return  response()->json(['msg' => 'No existe una caja aperturada para poder obtener información.'], 422);
            }
            $objeto = ['value' => $cash->value, 'close' => $cash->value_close, 'card' => $cash->value_card, 'cash' => $cash->value_cash, 'sales' => $cash->value_sales];
            return  response()->json(['results' => $objeto, 'msg' => 'Success'], 200);
        } catch (Exception $e) {
            return response()->json(['mensaje' => $e->getMessage()], 500);
        }
    }
    public function guardarCierreCaja(Request $request)
    {

        $request->validate([
            'value_cash' => 'numeric|integer',
            'value_card' => 'numeric|integer',
            'value_sales' => 'numeric|integer',
            'value_open' => 'required|numeric|integer',
            'value_close' => 'required|numeric|integer',

        ]);
        DB::beginTransaction();
        try {
            $dateTime = new DateTime();
            $fecha = $dateTime->format('Y-m-d');
            $time = $dateTime->format('H:i:s');
            $cajaActual = Cash::latest('date_open')->latest('hour_open')->whereNull('date_close')->first();
            if (!isset($cajaActual) || is_null($cajaActual)) {
                return  response()->json(['msg' => 'No existe una caja aperturada para ser cerrada'], 422);
            }
            $gastos = $request->input('expenses');
            $totalGastos = sizeof($gastos);
            if (isset($gastos) && $totalGastos > 0) {
                foreach ($gastos as $key => $gasto) {

                    Expenses::Create(
                        [
                            'cash_id' => $cajaActual->cash_id,
                            'name' => $gasto['name'],
                            'value' => is_null($gasto['value']) ? 0 : $gasto['value'],
                        ]
                    );
                }
            }

            Cash::latest('date_open')->latest('hour_open')->whereNull('date_close')->update(
                [
                    'date_close' => $fecha,
                    'hour_close' => $time,
                    'value_card' => is_null($request->input('value_card')) ? 0 : $request->input('value_card'),
                    'value_cash' => is_null($request->input('value_cash')) ? 0 : $request->input('value_cash'),
                    'value_close' => is_null($request->input('value_close')) ? 0 : $request->input('value_close'),
                    'value_open' => is_null($request->input('value_open')) ? 0 : $request->input('value_open'),
                    'value_sales' => is_null($request->input('value_sales')) ? 0 : $request->input('value_sales'),
                ]
            );


            DB::commit();
            return  response()->json(['results' => null, 'msg' => 'Información guardada con exito'], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['mensaje' => $e->getMessage()], 500);
        }
    }
    public function guardarMovimientos(Request $request)
    {


        try {

            $cajaActual = Cash::latest('date_open')->latest('hour_open')->whereNull('date_close')->first();
            if (!isset($cajaActual) || is_null($cajaActual)) {
                return  response()->json(['msg' => 'No existe una caja aperturada, no se puede procesar la petición'], 422);
            }
            $totalValueCard = is_null($cajaActual->value_card) ? 0 : $cajaActual->value_card;
            $totalValueCash = is_null($cajaActual->value_cash) ? 0 : $cajaActual->value_cash;
            $totalValueSales = is_null($cajaActual->value_sales) ? 0 : $cajaActual->value_sales;
            $movimientos = $request->input('movimientos');
            $totalMovimientos = sizeof($movimientos);
            if (isset($movimientos) && $totalMovimientos > 0) {
                foreach ($movimientos as $key => $movimiento) {

                    if ($movimiento['tipo_movimiento'] == 'Tarjeta') {
                        $totalValueCard = $totalValueCard + $movimiento['value'];
                    } elseif ($movimiento['tipo_movimiento'] == 'Efectivo') {
                        $totalValueCash = $totalValueCash + $movimiento['value'];
                    } elseif ($movimiento['tipo_movimiento'] == 'Venta') {
                        $totalValueSales = $totalValueSales + $movimiento['value'];
                    }
                }
                Cash::latest('date_open')->latest('hour_open')->whereNull('date_close')->update(
                    [

                        'value_card' => $totalValueCard,
                        'value_cash' => $totalValueCash,
                        'value_sales' => $totalValueSales
                    ]
                );
            } else {
                return  response()->json(['msg' => 'Debe ingresar movimientos para continuar'], 422);
            }
            return  response()->json(['results' => null, 'msg' => 'Información guardada con exito'], 200);
        } catch (Exception $e) {

            return response()->json(['mensaje' => $e->getMessage()], 500);
        }
    }
}
