<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
    protected $fillable = [
        'expenses_id',
        'cash_id',
        'name',
        'value',
    ];
    protected $table = 'expenses';
    protected $primaryKey = 'expenses_id';
}
